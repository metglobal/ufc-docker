Please refer to the following link for a detailed explanation. [How to use Docker for local web development: an update](http://tech.osteel.me/posts/2017/01/15/how-to-use-docker-for-local-web-development-an-update.html "How to use Docker for local web development: an update"). 

It contains a basic LEMP stack running with Docker, intented to be used for local web development.

## Get started

Install Docker on your machine using one of these three ways:

 - [Docker for Mac](https://docs.docker.com/docker-for-mac/)
 - [Docker for Windows](https://docs.docker.com/docker-for-windows/)
 - [Docker Toolbox](https://www.docker.com/products/docker-toolbox)

**How to install git**

   - [Git Download Link](https://git-scm.com/download/)

Create projects directive with all lowercase and clone all 5 repositories to this folder.

    $ mkdir projects

**And go to projects root**
    
    $ cd projects
    
