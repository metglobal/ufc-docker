FROM phusion/baseimage:master

ENV TZ=Etc/GMT-2
ENV SHELL=/bin/bash
ENV DEBIAN_FRONTEND="noninteractive"

# Ensure UTF-8
RUN echo 'tr_TR.UTF-8 UTF-8' >> /etc/locale.gen \
    && locale-gen en_US.UTF-8 tr_TR.UTF-8 
ENV LANG       en_US.UTF-8
ENV LC_ALL     en_US.UTF-8
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN add-apt-repository ppa:ondrej/php

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list

RUN apt-get update

RUN apt-get install -y libcurl4 libcurl4-openssl-dev pkg-config libssl-dev

RUN apt-get install -y --no-install-recommends\
    make\
	curl\
	nginx\
	php7.3\
    php7.3-common\
    php-pear\
	php7.3-fpm\
	php7.3-dev\
	php7.3-zip\
	php7.3-mysql\
	php7.3-imap\
	php7.3-json\
	php7.3-curl\
	php7.3-xml\
	php7.3-phar\
	php7.3-intl\
	php7.3-dom\
	php7.3-xmlreader\
	php7.3-ctype\
    php7.3-mbstring\
    php7.3-gd\
    php7.3-odbc\
    php7.3-bcmath\
    php-xdebug\
    php-mongodb\
    php-memcached\
    php-redis\
    composer

RUN ACCEPT_EULA=Y apt-get install -y msodbcsql17

RUN pecl install mongodb-1.5.0

RUN echo "extension=mongodb.so" > /etc/php/7.3/fpm/conf.d/20-mongodb.ini && \
    echo "extension=mongodb.so" > /etc/php/7.3/cli/conf.d/20-mongodb.ini && \
    echo "extension=mongodb.so" > /etc/php/7.3/mods-available/mongodb.ini

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /var/www
COPY ./xdebug.ini /etc/php/7.3/mods-available/xdebug.ini
COPY ./www.conf /etc/php/7.3/fpm/pool.d/www.conf
COPY ./php.ini /etc/php/7.3/fpm/php.ini
COPY ./php.ini /etc/php/7.3/cli/php.ini

ENV PATH /root/.composer/vendor/bin:$PATH

CMD ["/usr/sbin/php-fpm7.3", "-F", "-O", "-y", "/etc/php/7.3/fpm/pool.d/www.conf"]


